//
//  RecognitionContactsTableControllerViewController.m
//  DreamforceHackathon
//
//  Created by Vadim on 28.10.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import "RecognitionContactsTableControllerViewController.h"
#import "ContactsCell.h"
#import "ContactDAO.h"
#import "AppManager.h"
#import "ProfileController.h"
#import "Contact.h"
//#import "GHNSString+TimeInterval.h"
//#import <QuartzCore/QuartzCore.h>
#import "FacialRecognitionDAO.h"

@interface RecognitionContactsTableControllerViewController ()
{
    UIAlertView *alert;
    UIActivityIndicatorView *progress;
    UIAlertView *alertError;
    NSArray *sortedContact;
    
    AppManager *appMan;
    Contact *selectContact;
    
    NSMutableDictionary* contactsInfoDict;
}

@end

@implementation RecognitionContactsTableControllerViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Photo Library",@"Camera", nil];
    actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    if ([window.subviews containsObject:self.view]) {
        [actionSheet showInView:self.view];
    } else {
        [actionSheet showInView:window];
    }
    
    appMan = [AppManager instance];
    sortedContact = [[NSArray alloc] init];
    
    CGSize size = CGSizeMake(320, 44);
    UIColor* color = [UIColor colorWithRed:50.0/255 green:102.0/255 blue:147.0/255 alpha:1.0f];
    
    UIGraphicsBeginImageContext(size);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGRect fillRect = CGRectMake(0,0,size.width,size.height);
    CGContextSetFillColorWithColor(currentContext, color.CGColor);
    CGContextFillRect(currentContext, fillRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UINavigationBar* navAppearance = [UINavigationBar appearance];
    
    [navAppearance setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [navAppearance setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                           [UIColor whiteColor], UITextAttributeTextColor,
                                           [UIFont fontWithName:@"GillSans-Bold" size:18.0f], UITextAttributeFont, [NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)], UITextAttributeTextShadowOffset,
                                           nil]];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.tableContacts reloadData];
}

- (void) dismissAlert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    alert = nil;
}

-(NSArray*)sortAlphabet:(NSArray*)accountsSort{
    
    return [NSArray arrayWithArray:[accountsSort sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Contact *object1 = (Contact*)obj1;
        Contact *object2 = (Contact *)obj2;
        return [[object1 name] compare:[object2 name]];
    }]];
}

-(NSArray*)sortRecognize:(NSArray*)accountsSort{
    
    return [NSArray arrayWithArray:[accountsSort sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[NSNumber numberWithFloat:[[obj2 objectForKey:@"result"] floatValue]] compare:[NSNumber numberWithFloat:[[obj1 objectForKey:@"result"] floatValue]]];
    }]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sortedContact count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactsCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsCell"];
    cell.nameContactLabel.text = [NSString stringWithFormat:@"%@ %@",[[sortedContact objectAtIndex:indexPath.row] objectForKey:@"firstName"],[[sortedContact objectAtIndex:indexPath.row] objectForKey:@"lastName"]];
    cell.mailContact.text = [NSString stringWithFormat:@"SF ID: %@", [[sortedContact objectAtIndex:indexPath.row] objectForKey:@"sfId"]];
    [cell.recognitionPercentLabel setHidden:NO];
    [cell.recognitionPercentLabel setText:[[sortedContact objectAtIndex:indexPath.row] objectForKey:@"result"]];
    
    if ([[contactsInfoDict allKeys] containsObject:[[sortedContact objectAtIndex:indexPath.row] objectForKey:@"sfId"]])
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if ([[sortedContact objectAtIndex:indexPath.row] objectForKey:@"image"])
    {
        [cell.profileImage setHidden:NO];
        [cell.profileImage setImage:[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://162.222.183.142/getImage?url=images/%@&width=100&height=100",[[sortedContact objectAtIndex:indexPath.row] objectForKey:@"image"]]]]]];
    }
    else
    {
        [cell.profileImage setHidden:YES];
    }
    //cell.signalImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"signal-strength-bar-%i.png", 0]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryDisclosureIndicator)
    {
        selectContact = [contactsInfoDict objectForKey:[[sortedContact objectAtIndex:indexPath.row] objectForKey:@"sfId"]];
        [self performSegueWithIdentifier:@"showProfile" sender:self];
    }
    else
    {
        UIAlertView *notInfoAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Info is not available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [notInfoAlert show];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showProfile"]) {
        ProfileController *profileContact = (ProfileController*)segue.destinationViewController;
        profileContact.contact = selectContact;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
    UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    
    alert = [[UIAlertView alloc] initWithTitle: @"Uploading..." message: nil delegate:self cancelButtonTitle: nil otherButtonTitles: nil];
    progress= [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(125, 50, 30, 30)];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [alert addSubview: progress];
    [progress startAnimating];
    [alert show];
    
    [[FacialRecognitionDAO instance] runFaceRecognitionForImage:UIImageJPEGRepresentation(image, 1.0) Response:^(NSString *status, NSArray *data) {
        if ([status isEqualToString:@"progress"])
        {
            int progressInPercent = [[[data objectAtIndex:0] objectForKey:@"progress"] intValue];
            if (progressInPercent < 100)
            {
                [alert setTitle:[NSString stringWithFormat:@"Uploading...%d%%", progressInPercent]];
            }
            else
            {
                [alert setTitle:@"Recognizing..."];
            }
        }
        else
        {
            NSLog(@"upload %@: response = %@", status, data);
            if ([status isEqualToString:@"success"])
            {
                [alert setTitle:@"Recognize successful"];
                sortedContact = [self sortRecognize:data];
                contactsInfoDict = [[NSMutableDictionary alloc] init];
                NSArray* tmpContactsArray = [[NSArray alloc] initWithArray:[[ContactDAO instance] getContactsFromCoreData]];
                NSLog(@"tmpContactsArray = %@", tmpContactsArray);
                for (Contact* contact in tmpContactsArray)
                {
                    [contactsInfoDict setObject:contact forKey:contact.idKey];
                }
            }
            else
            {
                [alert setTitle:@"No matches found"];
            }
            [self.tableContacts reloadData];
            [progress stopAnimating];
            progress.hidden = YES;
            [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:1.0];
        }
    } Failure:^{
        NSLog(@"upload fail!");
        [progress stopAnimating];
        [alert setTitle:@"Recognize fail"];
        progress.hidden = YES;
        [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:1.0];
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self photoLibraryButtonClick];
        
    } else if (buttonIndex == 1){
        [self cameraButtonClick];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)photoLibraryButtonClick {
    UIImagePickerController *imagePicker = [[UIImagePickerController     alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
}

- (void)cameraButtonClick {
    UIImagePickerController *imagePicker = [[UIImagePickerController     alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
}


@end
