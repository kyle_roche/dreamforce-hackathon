//
//  AppDelegate.h
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
