//
//  ProfileController.h
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface ProfileController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *faxLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (nonatomic, strong) UISwipeGestureRecognizer *swipeGesture;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel * sensorIdLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *infoScroll;
@property (weak, nonatomic) IBOutlet UITextField *sensorIdField;

@property (nonatomic, weak)  Contact* contact;
- (IBAction)changeSensorId:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;
- (IBAction)addPhotoButtonClick:(id)sender;



@end
