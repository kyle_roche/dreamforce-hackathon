//
//  RecognitionContactsTableControllerViewController.h
//  DreamforceHackathon
//
//  Created by Vadim on 28.10.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecognitionContactsTableControllerViewController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableContacts;

@end
