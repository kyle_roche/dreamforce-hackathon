//
//  ContactsTableController.h
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FYX/FYX.h>
//#import <FYX/FYXSightingManager.h>
//#import <FYX/FYXTransmitter.h>
@protocol ContactsControllerDelegate <NSObject>
-(void)login;
@end

@interface ContactsTableController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableContacts;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *logoutButton;

@property (weak, nonatomic) id<ContactsControllerDelegate> delegate;

- (IBAction)recognotionButtonClick:(id)sender;


@end
