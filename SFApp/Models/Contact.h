//
//  Contact.h
//  Playbook
//
//  Created by Dmity on 21.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Contact : NSManagedObject

    @property (nonatomic, retain) NSString *idKey;
    @property (nonatomic, retain) NSString *checkDeleted;
    @property (nonatomic, retain) NSString *masterRecordId;
    @property (nonatomic, retain) NSString *accountId;
    @property (nonatomic, retain) NSString *lastName;
    @property (nonatomic, retain) NSString *firstName;
    @property (nonatomic, retain) NSString *salutation;
    @property (nonatomic, retain) NSString *name;
    @property (nonatomic, retain) NSString *otherStreet;
    @property (nonatomic, retain) NSString *otherCity;
    @property (nonatomic, retain) NSString *otherState;
    @property (nonatomic, retain) NSString *otherPostalCode;
    @property (nonatomic, retain) NSString *otherCountry;
    @property (retain, nonatomic) NSString *mailingStreet;
    @property (retain, nonatomic) NSString *mailingCity;
    @property (retain, nonatomic) NSString *mailingState;
    @property (retain, nonatomic) NSString *mailingPostalCode;
    @property (retain, nonatomic) NSString *mailingCountry;
    @property (retain, nonatomic) NSString *phone;
    @property (retain, nonatomic) NSString *fax;
    @property (retain, nonatomic) NSString *mobilePhone;
    @property (retain, nonatomic) NSString *homePhone;
    @property (retain, nonatomic) NSString *otherPhone;
    @property (retain, nonatomic) NSString *assistantPhone;
    @property (retain, nonatomic) NSString *reportsToId;
    @property (retain, nonatomic) NSString *email;
    @property (retain, nonatomic) NSString *title;
    @property (retain, nonatomic) NSString *department;
    @property (retain, nonatomic) NSString *assistantName;
    @property (retain, nonatomic) NSString *leadSource;
    @property (retain, nonatomic) NSString *birthdate;
    @property (retain, nonatomic) NSString *descriptionContact;
    @property (retain, nonatomic) NSString *ownerId;
    @property (retain, nonatomic) NSString *createdDate;
    @property (retain, nonatomic) NSString *createdById;
    @property (retain, nonatomic) NSString *lastModifiedDate;
    @property (retain, nonatomic) NSString *lastModifiedById;
    @property (retain, nonatomic) NSString *systemModstamp;
    @property (retain, nonatomic) NSString *lastActivityDate;
    @property (retain, nonatomic) NSString *lastCURequestDate;
    @property (retain, nonatomic) NSString *lastCUUpdateDate;
    @property (retain, nonatomic) NSString *emailBouncedReason;
    @property (retain, nonatomic) NSString *emailBouncedDate;
    @property (retain, nonatomic) NSString *jigsaw;
    @property (retain, nonatomic) NSString *jigsawContactId;
    @property (retain, nonatomic) NSData * avatarImageData;
    @property (retain, nonatomic) NSString * sensorID;
    @property (retain, nonatomic) NSString * userId;


    - (id)initWithDictionary:(NSDictionary*)dictContact andUserId:(NSString*)userId;

@end
