//
//  Contact.m
//  Playbook
//
//  Created by Dmity on 21.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import "Contact.h"
#import "AppManager.h"
#import "CommonDataManager.h"
//#import "AmazonDAO.h"

@implementation Contact
@dynamic idKey;
@dynamic checkDeleted;
@dynamic masterRecordId;
@dynamic accountId;
@dynamic lastName;
@dynamic firstName;
@dynamic salutation;
@dynamic name;
@dynamic otherStreet;
@dynamic otherCity;
@dynamic otherState;
@dynamic otherPostalCode;
@dynamic otherCountry;
@dynamic mailingStreet;
@dynamic mailingCity;
@dynamic mailingState;
@dynamic mailingPostalCode;
@dynamic mailingCountry;
@dynamic phone;
@dynamic fax;
@dynamic mobilePhone;
@dynamic homePhone;
@dynamic otherPhone;
@dynamic assistantPhone;
@dynamic reportsToId;
@dynamic email;
@dynamic title;
@dynamic department;
@dynamic assistantName;
@dynamic leadSource;
@dynamic birthdate;
@dynamic descriptionContact;
@dynamic ownerId;
@dynamic createdDate;
@dynamic createdById;
@dynamic lastModifiedDate;
@dynamic lastModifiedById;
@dynamic systemModstamp;
@dynamic lastActivityDate;
@dynamic lastCURequestDate;
@dynamic lastCUUpdateDate;
@dynamic emailBouncedReason;
@dynamic emailBouncedDate;
@dynamic jigsaw;
@dynamic jigsawContactId;
@dynamic avatarImageData;
@dynamic  sensorID;
@dynamic userId;


- (id)initWithDictionary:(NSDictionary*)dictContact andUserId:(NSString*)userId1{
    
    Contact * newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:[[CommonDataManager instance] managedObjectContext]];
    
    newContact.idKey = [dictContact objectForKey:@"Id"];
    newContact.checkDeleted = [dictContact objectForKey:@"IsDeleted"];
    newContact.masterRecordId = [dictContact objectForKey:@"MasterRecordId"];
    newContact.accountId = [dictContact objectForKey:@"AccountId"];
    newContact.lastName = [dictContact objectForKey:@"LastName"];
    newContact.firstName = [dictContact objectForKey:@"FirstName"];
    newContact.salutation = [dictContact objectForKey:@"Salutation"];
    newContact.name = [dictContact objectForKey:@"Name"];
    newContact.otherStreet = [dictContact objectForKey:@"OtherStreet"];
    newContact.otherCity = [dictContact objectForKey:@"OtherCity"];
    newContact.otherState = [dictContact objectForKey:@"OtherState"];
    newContact.otherPostalCode = [dictContact objectForKey:@"OtherPostalCode"];
    newContact.otherCountry = [dictContact objectForKey:@"OtherCountry"];
    newContact.mailingStreet = [dictContact objectForKey:@"MailingStreet"];
    newContact.mailingCity = [dictContact objectForKey:@"MailingCity"];
    newContact.mailingState = [dictContact objectForKey:@"MailingState"];
    newContact.mailingPostalCode = [dictContact objectForKey:@"MailingPostalCode"];
    newContact.mailingCountry = [dictContact objectForKey:@"MailingCountry"];
    newContact.phone = [dictContact objectForKey:@"Phone"];
    newContact.fax = [dictContact objectForKey:@"Fax"];
    newContact.mobilePhone = [dictContact objectForKey:@"MobilePhone"];
    newContact.homePhone = [dictContact objectForKey:@"HomePhone"];
    newContact.otherPhone = [dictContact objectForKey:@"OtherPhone"];
    newContact.assistantPhone = [dictContact objectForKey:@"AssistantPhone"];
    newContact.reportsToId = [dictContact objectForKey:@"ReportsToId"];
    newContact.email = [dictContact objectForKey:@"Email"];
    newContact.title = [dictContact objectForKey:@"Title"];
    newContact.department = [dictContact objectForKey:@"Department"];
    newContact.assistantName = [dictContact objectForKey:@"AssistantName"];
    newContact.leadSource = [dictContact objectForKey:@"LeadSource"];
    newContact.birthdate = [dictContact objectForKey:@"Birthdate"];
    newContact.descriptionContact = [dictContact objectForKey:@"Description"];
    newContact.ownerId = [dictContact objectForKey:@"OwnerId"];
    newContact.createdDate = [dictContact objectForKey:@"CreatedDate"];
    newContact.createdById = [dictContact objectForKey:@"CreatedById"];
    newContact.lastModifiedDate = [dictContact objectForKey:@"LastModifiedDate"];
    newContact.lastModifiedById = [dictContact objectForKey:@"LastModifiedById"];
    newContact.systemModstamp = [dictContact objectForKey:@"SystemModstamp"];
    newContact.lastActivityDate = [dictContact objectForKey:@"LastActivityDate"];
    newContact.lastCURequestDate = [dictContact objectForKey:@"LastCURequestDate"];
    newContact.lastCUUpdateDate = [dictContact objectForKey:@"LastCUUpdateDate"];
    newContact.emailBouncedReason = [dictContact objectForKey:@"EmailBouncedReason"];
    newContact.emailBouncedDate = [dictContact objectForKey:@"EmailBouncedDate"];
    newContact.jigsaw = [dictContact objectForKey:@"Jigsaw"];
    newContact.jigsawContactId = [dictContact objectForKey:@"JigsawContactId"];
    NSURL *url = [[AppManager instance] getGravatarURL:[dictContact objectForKey:@"Email"]];
    newContact.avatarImageData = [NSData dataWithContentsOfURL:url];
    
    
    newContact.userId = userId1;
    
    return newContact;
}

@end
