//
//  ContactsTableController.m
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import "ContactsTableController.h"
#import "ContactsCell.h"
#import "ContactDAO.h"
#import "AppManager.h"
#import "ProfileController.h"
#import "Contact.h"
//#import "GHNSString+TimeInterval.h"
//#import <QuartzCore/QuartzCore.h>
#import "FacialRecognitionDAO.h"
#import "RecognitionContactsTableControllerViewController.h"

@interface ContactsTableController () {

    NSArray * contactsList;
    UIAlertView *alert;
    UIActivityIndicatorView *progress;
    UIAlertView *alertError;
    NSArray *sortedContact;
    
    AppManager *appMan;
    Contact *selectContact;
}
//@property (nonatomic) FYXSightingManager *sightingManager;
@end

@implementation ContactsTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    appMan = [AppManager instance];
    sortedContact = [[NSArray alloc] init];
    
//    [FYX startService:self];
//    self.sightingManager = [FYXSightingManager new];
//    self.sightingManager.delegate = self;
//    [self.sightingManager scan];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull Down to Refresh"];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
    CGSize size = CGSizeMake(320, 44);
    UIColor* color = [UIColor colorWithRed:50.0/255 green:102.0/255 blue:147.0/255 alpha:1.0f];
    
    UIGraphicsBeginImageContext(size);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGRect fillRect = CGRectMake(0,0,size.width,size.height);
    CGContextSetFillColorWithColor(currentContext, color.CGColor);
    CGContextFillRect(currentContext, fillRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UINavigationBar* navAppearance = [UINavigationBar appearance];
    
    [navAppearance setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [navAppearance setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                           [UIColor whiteColor], UITextAttributeTextColor,
                                           [UIFont fontWithName:@"GillSans-Bold" size:18.0f], UITextAttributeFont, [NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)], UITextAttributeTextShadowOffset,
                                           nil]];
    
    contactsList = [[NSMutableArray alloc] init];
    alert = [[UIAlertView alloc] initWithTitle: @"Loading..." message: nil delegate:self cancelButtonTitle: nil otherButtonTitles: nil];
    progress= [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(125, 50, 30, 30)];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [alert addSubview: progress];
    [progress startAnimating];
    [alert show];
    //GET ALL CONTACTS
    
    [self getContactsWithRwfresh:NO];
    if (!contactsList.count) {
        [self getContactsWithRwfresh:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.tableContacts reloadData];
}

-(void)getContactsWithRwfresh:(BOOL)refresh
{
    if (refresh) {
        [[ContactDAO instance] getAllContacts:^(NSString *status, NSArray *resultData) {
            
            if ([status isEqual:@"success"]) {
                contactsList = [[ContactDAO instance] getContactsFromCoreData];
                sortedContact = [self sortAlphabet:contactsList];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableContacts reloadData];
                });
            }
            [progress stopAnimating];
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            progress.hidden = YES;
            alert = nil;
            
            [self.refreshControl endRefreshing];
            self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull Down to Refresh"];
            
            
        } Failure:^{
            [progress stopAnimating];
            [alert dismissWithClickedButtonIndex:0 animated:YES];
        }];
    }else{
        contactsList = [[ContactDAO instance] getContactsFromCoreData];
        sortedContact = [self sortAlphabet:contactsList];
        if (contactsList.count) {
            [progress stopAnimating];
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            progress.hidden = YES;
            alert = nil;
        }
        
        [self.tableContacts reloadData];
    }
    
    

    
}

-(NSArray*)sortAlphabet:(NSArray*)accountsSort{
    
    return [NSArray arrayWithArray:[accountsSort sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Contact *object1 = (Contact*)obj1;
        Contact *object2 = (Contact *)obj2;
        return [[object1 name] compare:[object2 name]];
    }]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [sortedContact count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContactsCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsCell"];
    cell.nameContactLabel.text = [[sortedContact objectAtIndex:indexPath.row] name];
    cell.mailContact.text = [[sortedContact objectAtIndex:indexPath.row] email];
    
    if([[[sortedContact objectAtIndex:indexPath.row] lastActivityDate] isEqualToString:@" "]) {
        cell.timeLabel.text = @"Was never active";
    }
    else {
        NSDate *date = [appMan adjustmentDateFromDate:@"yyyy-MM-dd" date:[[sortedContact objectAtIndex:indexPath.row] lastActivityDate]];
        
        NSTimeInterval dif = [date timeIntervalSinceNow];
        //cell.timeLabel.text = [NSString stringWithFormat:@"%@ ago", [NSString gh_stringForTimeInterval:dif includeSeconds:YES]];
    }
    
    [cell.profileImage setImage:[[UIImage alloc] initWithData:[[sortedContact objectAtIndex:indexPath.row] avatarImageData]]];
    
    cell.signalImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"signal-strength-bar-%i.png", 0]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectContact = [sortedContact objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"showProfile" sender:self];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showProfile"]) {
        ProfileController *profileContact = (ProfileController*)segue.destinationViewController;
        profileContact.contact = selectContact;
    }
    if([segue.identifier isEqualToString:@"recognition"]) {
        RecognitionContactsTableControllerViewController *recognitionContactsTable = (RecognitionContactsTableControllerViewController*)segue.destinationViewController;
        //profileContact.contact = selectContact;
    }
}

- (IBAction)logoutClicked:(id)sender {
    [appMan.coordinator revokeAuthentication];
    [self.navigationController popViewControllerAnimated:YES];
    
    [self.delegate login];
    
}

-(void)refreshView:(UIRefreshControl *)refresh {
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    [self getContactsWithRwfresh:YES];
}


- (void)serviceStarted
{
    NSLog(@"FYX Service Started");
}

- (void)startServiceFailed:(NSError *)error
{
    NSLog(@"%@", error);
}

//- (void)didReceiveSighting:(FYXTransmitter *)transmitter time:(NSDate *)time RSSI:(NSNumber *)RSSI
//{
//
//    //NSLog(@"I received an FYX sighting from %@ - %@ - %@ - %@ - %i", transmitter.name, transmitter.identifier, transmitter.temperature, transmitter.battery,[self getSignalStrengthFordB: RSSI.intValue]);
//    
//    
//    for(int i=0;i<[sortedContact count];i++){
//       // NSLog(@"sensor:%@",[[sortedContact objectAtIndex:i] sensorID]);
//        if([[[sortedContact objectAtIndex:i] sensorID] isEqualToString: transmitter.identifier]){
//           // NSLog(@"sensor:%@ row:%i",transmitter.identifier,i);
//            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//            ContactsCell *cell = (ContactsCell*)[self.tableContacts cellForRowAtIndexPath:indexPath];
//            cell.signalImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"signal-strength-bar-%i.png", [self getSignalStrengthFordB: RSSI.intValue] ]];
//           // NSArray* indexArray = [NSArray arrayWithObjects:indexPath, nil];
//            // Launch reload for the two index path
//            //[self.tableContacts reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
//        }
//    }
//    
//    
//}

//- (int) getSignalStrengthFordB:(int) rssi{
//    if (rssi>-65) {
//        return 5;
//    }else{
//        if (rssi>-75) {
//            return 4;
//        }else{
//            if (rssi>-85) {
//                return 3;
//            }else{
//                if (rssi>-95){
//                    return 2;
//                }else{
//                    if (rssi>-105) {
//                        return 1;
//                    }else return 0;
//                }
//                
//            }
//        }
//    }
//}

- (IBAction)recognotionButtonClick:(id)sender {
    [self performSegueWithIdentifier:@"recognition" sender:self];
}

@end
