//
//  ViewController.m
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import "iPhone_LoginViewController.h"
#import "SFAccountManager.h"
#import "AppManager.h"
#import "ContactsTableController.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"

static NSString *const remoteAccessConsumerKey = @"3MVG9yZ.WNe6byQBiG4Z3Oaru7ZFRFdy1GV_PAqWLLRC7L1bavtVZitVx3N0YdiY2yyp6YLpsWhp1aOuv8iOV";
static NSString *const OAuthRedirectURI = @"https://login.salesforce.com/services/oauth2/success";
static NSString *const OAuthLoginDomain = @"login.salesforce.com";
NSString * const kLoginHostUserDefault1 = @"login_host_pref";

@interface iPhone_LoginViewController (){
    SFAccountManager *_accountMgr;
    UIActivityIndicatorView *progress;
    UIAlertView *alert;
    AppManager *_appMan;
}

@end

@implementation iPhone_LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    _appMan = [AppManager instance];
    [self login];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)login {
    
    alert = [[UIAlertView alloc] initWithTitle: @"Loading..." message: nil delegate:self cancelButtonTitle: nil otherButtonTitles: nil];
    progress= [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(125, 50, 30, 30)];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [alert addSubview: progress];
    [progress startAnimating];
    [alert show];
    NSString *loginDomain = [self oauthLoginDomain];
    NSString *accountIdentifier = [self userAccountIdentifier];
    
    [SFAccountManager setCurrentAccountIdentifier:accountIdentifier];
    [SFAccountManager setLoginHost:(loginDomain ? loginDomain : @"login.salesforce.com")];
    [SFAccountManager setClientId:remoteAccessConsumerKey];
    
    [SFAccountManager setRedirectUri:OAuthRedirectURI];
    [SFAccountManager setScopes:[NSSet setWithObjects:@"web",@"api",nil]];
    
    _accountMgr = [SFAccountManager sharedInstance];
    
    _accountMgr.oauthDelegate = self;
    
    SFOAuthCredentials *credentials = _accountMgr.credentials;
    
    credentials.domain = OAuthLoginDomain;
    credentials.redirectUri = OAuthRedirectURI;
    
    if(!_appMan.coordinator){
        _appMan.coordinator = [[SFOAuthCoordinator alloc] initWithCredentials:credentials];
    }
    _appMan.coordinator.delegate = self;
    //    if(signUp) {
    //        [_appManager.coordinator revokeAuthentication];
    //    }
    
    [_appMan.coordinator authenticate];
    
}

- (NSString*)oauthLoginDomain {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
	NSString *loginHost = [defs objectForKey:kLoginHostUserDefault1];
    
    return loginHost;
}

- (NSString*)userAccountIdentifier {
    return @"Default";
}

#pragma mark - SFOAuthCoordinatorDelegate
- (void)oauthCoordinator:(SFOAuthCoordinator *)coordinator willBeginAuthenticationWithView:(UIWebView *)view {
    NSLog(@"oauthCoordinator:willBeginAuthenticationWithView");
    
    //[self.loading startAnimating];
}

- (void)oauthCoordinator:(SFOAuthCoordinator *)coordinator didBeginAuthenticationWithView:(UIWebView *)view {
    NSLog(@"oauthCoordinator:didBeginAuthenticationWithView");
    
    [view setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    //[view setFrame:self.view.bounds];
    [self.view addSubview:view];
    [view setFrame:CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height)];
    [progress stopAnimating];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    progress.hidden = YES;
    alert = nil;
    //[self.loading stopAnimating];
}

- (void)oauthCoordinatorDidAuthenticate:(SFOAuthCoordinator *)coordinator authInfo:(SFOAuthInfo *)info{
    NSLog(@"oauthCoordinatorDidAuthenticate with sessionid: %@, userId: %@", coordinator.credentials.accessToken, coordinator.credentials.userId);
    //[coordinator.view removeFromSuperview];
    //[[SFRestAPI sharedInstance] setCoordinator:coordinator];
    //[SFAccountManager sharedInstance].credentials = coordinator.credentials;
    
    _appMan.SFDC_userId = coordinator.credentials.userId;

    [progress stopAnimating];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    [self performSegueWithIdentifier:@"showContacts" sender:self];
    
}



- (void)oauthCoordinator:(SFOAuthCoordinator *)coordinator didFailWithError:(NSError *)error authInfo:(SFOAuthInfo *)info{
    [coordinator.view removeFromSuperview];
    NSLog(@"error = %@", error);
    NSLog(@"authInfo = %@", info);
    [_appMan.coordinator revokeAuthentication];
    [progress stopAnimating];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    progress.hidden = YES;
    alert = nil;
    [self login];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showContacts"]) {
        ContactsTableController * contactController = (ContactsTableController*)(segue.destinationViewController);
        contactController.delegate = self;
    }
}

@end
