//
//  ViewController.h
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFOAuthCoordinator.h"
#import "ContactsTableController.h"

@interface iPhone_LoginViewController : UIViewController<SFOAuthCoordinatorDelegate, ContactsControllerDelegate>
    - (void)login;
@end
