//
//  ContactDAO.h
//  Playbook
//
//  Created by Dmity on 21.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFRestRequest.h"
#import "Contact.h"

@interface ContactDAO : NSObject <SFRestDelegate>


+ (ContactDAO *)instance;
- (void) initData;
-(NSArray*)getContactsFromCoreData;
- (void) getAllContacts: (void (^)(NSString*,NSArray*))callbackBlock Failure:(void (^)())failure;
- (Contact*) getContactInfoById:(NSString*)idKey;
-(void) deleteContact: (NSString *)objectType WithId:(NSString *)objectId;
-(void) addContactOrAccount: (NSString *)contact dictionary:(NSMutableDictionary *)field;
-(void) editContact: (NSString *)objectType WithKeyId:(NSString *)objectId dictionary:(NSMutableDictionary *)field;

@end


