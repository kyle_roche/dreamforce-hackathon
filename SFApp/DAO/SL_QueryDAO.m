//
//  SL_QueryDAO.m
//  Playbook
//
//  Created by Dmity on 21.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import "SL_QueryDAO.h"
#import "AppManager.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"

@interface SL_QueryDAO(){
    AppManager *appMan;
}
@end


@implementation SL_QueryDAO

static SL_QueryDAO* _manager = nil;

+ (SL_QueryDAO *)instance {
    if (!_manager) {
        _manager = [[SL_QueryDAO alloc] init];
        [_manager initData];
    }
    return _manager;
}

- (void) initData{
    appMan = [AppManager instance];
}

-(void)getInfoForSalesforceURL:(NSString*) url params: (NSDictionary*) dict Response:(void (^)(id))callbackBlock Failure:(void (^)())failure {
    
    NSLog(@"URL: %@",url);
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL: appMan.coordinator.credentials.instanceUrl];
    
    NSLog(@"userId: %@  ClientId:%@  Password  %@",appMan.coordinator.credentials.userId,appMan.coordinator.credentials.clientId,appMan.coordinator.credentials.accessToken);
    
    NSDictionary * params = [NSMutableDictionary dictionaryWithDictionary:dict];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:url parameters:dict];
    
    NSLog(@"REQUEST: %@",request);
    
    [request setValue:[NSString stringWithFormat:@"OAuth %@",appMan.coordinator.credentials.accessToken] forHTTPHeaderField:@"Authorization"];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        callbackBlock(JSON);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"FAILED General Request: %@ - %@ - %@", [request URL], [request allHTTPHeaderFields], error.description);
        failure();
    }];
    [operation start];
}

@end
