//
//  FacialRecognitionDAO.m
//  DreamforceHackathon
//
//  Created by Vadim on 28.10.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import "FacialRecognitionDAO.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"

@implementation FacialRecognitionDAO

static FacialRecognitionDAO* _manager = nil;

+ (FacialRecognitionDAO *)instance {
    if (!_manager) {
        _manager = [[FacialRecognitionDAO alloc] init];
    }
    return _manager;
}

- (void)sendRequestWithUrl:(NSURL*)requestURL withImageData:(NSData*)imageData withParams:(NSDictionary*)params withUsername:(NSString*)username withPassword:(NSString*)password Response:(void (^)(id response))callbackBlock Failure:(void (^)())failure {
    /*
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    */
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *boundary = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'image'.
    NSString* FileParamConstant = @"image";
    
    // set Content-Type in HTTP header
    //NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    //[request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    if (params)
    {
        for (NSString *param in params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    /*
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:requestURL];
    
    // make the connection to the web
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError)
        {
            callbackBlock(data);
        }
        else
        {
            NSLog(@"FAILED upload image: %@ - %@ - %@", [request URL], [request allHTTPHeaderFields], connectionError.description);
            failure();
        }
    }];
    */
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://8.35.193.36/api/"]];
    
    NSMutableURLRequest *AFrequest = [client requestWithMethod:@"POST" path:[[[requestURL absoluteString] componentsSeparatedByString:@"/"] lastObject] parameters:nil];
    [AFrequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
    [AFrequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [AFrequest setHTTPShouldHandleCookies:NO];
    [AFrequest setTimeoutInterval:30];
    [AFrequest setHTTPBody:body];
    
    //[request setValue:[NSString stringWithFormat:@"OAuth %@",appMan.coordinator.credentials.accessToken] forHTTPHeaderField:@"Authorization"];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:AFrequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success");
        callbackBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"FAILED General Request: %@ - %@ - %@", [AFrequest URL], [AFrequest allHTTPHeaderFields], error.description);
        failure();
    }];
    
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite) {
        NSDictionary* responseDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"progress", @"status", [NSString stringWithFormat:@"%d", (int)(((float)totalBytesWritten/totalBytesExpectedToWrite)*100)], @"message", nil];
        NSError *error;
        callbackBlock([NSJSONSerialization dataWithJSONObject:responseDict options:NSJSONWritingPrettyPrinted error:&error]);
    }];
    
    [operation start];
    
}

- (void)uploadImageToServer:(NSData*)imageData withParams:(NSDictionary*)params Response:(void (^)(NSString* status,NSDictionary* data))callbackBlock Failure:(void (^)())failure {

    NSURL *requestURL = [NSURL URLWithString:@"http://8.35.193.36/api/uploadImage"];
    //NSURL *requestURL = [NSURL URLWithString:@"http://192.168.1.92/api/uploadImage"];
    [self sendRequestWithUrl:requestURL withImageData:imageData withParams:params withUsername:nil withPassword:nil Response:^(id response) {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil];
        if ([[result objectForKey:@"status"] isEqualToString:@"progress"])
        {
            callbackBlock([result objectForKey:@"status"], [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"message"], @"progress", nil]);
        }
        else
        {
            NSLog(@"result uploadImageToServer = %@", result);
            callbackBlock([result objectForKey:@"status"], [result objectForKey:@"message"]);
        }
    } Failure:^{
        failure();
    }];
}

- (void)runFaceRecognitionForImage:(NSData*)imageData Response:(void (^)(NSString* status,NSArray* data))callbackBlock Failure:(void (^)())failure {
    
    NSURL *requestURL = [NSURL URLWithString:@"http://8.35.193.36/api/uploadRecognize"];
    //NSURL *requestURL = [NSURL URLWithString:@"http://192.168.1.92/api/uploadRecognize"];
    [self sendRequestWithUrl:requestURL withImageData:imageData withParams:nil withUsername:nil withPassword:nil Response:^(id response) {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil];
        if ([[result objectForKey:@"status"] isEqualToString:@"progress"])
        {
            callbackBlock([result objectForKey:@"status"], [[NSArray alloc] initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"message"], @"progress", nil], nil]);
        }
        else
        {
            NSLog(@"result runFaceRecognitionForImage = %@", result);
            callbackBlock([result objectForKey:@"status"], [result objectForKey:@"message"]);
        }
    } Failure:^{
        failure();
    }];
}

@end
