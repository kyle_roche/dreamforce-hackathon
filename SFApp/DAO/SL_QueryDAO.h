//
//  SL_QueryDAO.h
//  Playbook
//
//  Created by Dmity on 21.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SL_QueryDAO  : NSObject
+ (SL_QueryDAO *)instance;
- (void)initData;
-(void)getInfoForSalesforceURL:(NSString*) url params: (NSDictionary*) dict Response:(void (^)(id))callbackBlock Failure:(void (^)())failure ;

@end
