//
//  ContactDAO.m
//  Playbook
//
//  Created by Dmity on 21.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import "ContactDAO.h"
#import "SL_QueryDAO.h"
#import "AppManager.h"
#import "SFRestRequest.h"
#import "SFRestAPI.h"
#import "Contact.h"
//#import "iPad_AccountsViewController.h"
#import "CommonDataManager.h"


@interface ContactDAO(){
    AppManager *appMan;
    SL_QueryDAO *slQuery;
    SFRestRequest *contactsStructureRequest;
    NSString *errorMessage;
}
@end


@implementation ContactDAO

static ContactDAO* _manager = nil;

+ (ContactDAO *)instance {
    if (!_manager) {
        _manager = [[ContactDAO alloc] init];
        [_manager initData];
    }
    return _manager;
}

-(void) initData{
    appMan = [AppManager instance];
    slQuery = [SL_QueryDAO instance];
}

-(NSArray*)getContactsFromCoreData{
    NSArray * oldContactsFromCD = [[CommonDataManager instance] getAllModelsFromCoreData:@"Contact" withRequest:[NSString stringWithFormat:@"(userId=='%@')",appMan.coordinator.credentials.userId] inContext:[[CommonDataManager instance] managedObjectContext]];
    return oldContactsFromCD;
}


- (void) getAllContacts: (void (^)(NSString*,NSArray*))callbackBlock Failure:(void (^)())failure{
    [self createRequestContacts:^(NSString *fieldsRequest) {
        NSString *requestText = [NSString stringWithFormat:@"SELECT %@ FROM Contact",fieldsRequest];
        
        SFRestRequest* _contactsRequest = [[SFRestAPI sharedInstance] requestForQuery:requestText];
        
        [slQuery getInfoForSalesforceURL:[NSString stringWithFormat:@"%@%@",_contactsRequest.endpoint, _contactsRequest.path] params:_contactsRequest.queryParams Response:^(id JSON) {
            NSMutableDictionary *tmpContact = [[NSMutableDictionary alloc] init];
            NSMutableArray *tmpContactsAr = [[NSMutableArray alloc] init];
            NSManagedObjectContext * context = [[CommonDataManager instance] createNewContextForCurrentThread];
            
            NSArray * oldContactsFromCD = [[CommonDataManager instance] getAllModelsFromCoreData:@"Contact" withRequest:[NSString stringWithFormat:@"(userId=='%@')",appMan.coordinator.credentials.userId] inContext:context];
//            NSLog(@"---");
//            NSLog(@"%@",JSON);
//            NSLog(@"---");
            [[CommonDataManager instance] removeModelsFromCoreDataNoSave:oldContactsFromCD FromContext:context];
            for (tmpContact in [JSON objectForKey:@"records"]){
                Contact *tmp = [[Contact alloc] initWithDictionary:[appMan checkNullWithDict:tmpContact] andUserId:appMan.coordinator.credentials.userId];
                [tmpContactsAr addObject: tmp];
               // NSLog(@"tmpContact = %@",tmpContact);
            }
            
            [context performBlockAndWait:^{
                NSError *error = nil;
                [context save:&error];
                NSLog(@"Error: %@",error);
                
            }];
            
            [[CommonDataManager instance].managedObjectContext performBlockAndWait:^{
                NSError *parentError = nil;
                [[CommonDataManager instance].managedObjectContext save:&parentError];
                NSLog(@"Parent Error: %@",parentError);
            }];
            callbackBlock(@"success",tmpContactsAr);
        } Failure:^{
            NSLog(@"FAILURE GET CONTACTS");
            callbackBlock(@"fail", [[NSArray alloc] init]);
        }];
    } Failure:^{
        NSLog(@"FAIL CREATE REQUEST FOR CONTACTS");
        callbackBlock(@"fail", [[NSArray alloc] init]);
    }];
}

- (Contact*) getContactInfoById:(NSString*)idKey{
    NSManagedObjectContext * context = [[CommonDataManager instance] createNewContextForCurrentThread];
    NSArray* contactArray = [[CommonDataManager instance] getAllModelsFromCoreData:@"Contact" withRequest:[NSString stringWithFormat:@"(idKey=='%@')",idKey] inContext:context];
    NSLog(@"Contact exist in Core Data = %d", [contactArray count]);
    if ([contactArray count])
    {
        Contact* contact = [contactArray objectAtIndex:0];
        return contact;
    }
    else
    {
        /*
        [self createRequestContacts:^(NSString *fieldsRequest) {
            NSString *requestText = [NSString stringWithFormat:@"SELECT %@ FROM Contact WHERE Id='%@'", fieldsRequest, accountId];
            SFRestRequest* _contactsRequest = [[SFRestAPI sharedInstance] requestForQuery:requestText];
            [slQuery getInfoForSalesforceURL:[NSString stringWithFormat:@"%@%@",_contactsRequest.endpoint, _contactsRequest.path] params:_contactsRequest.queryParams Response:^(id JSON) {
                //NSLog(@"%@",JSON);
                Contact *contact = [[Contact alloc] initWithDictionary:[appMan checkNullWithDict:[[JSON objectForKey:@"records"] objectAtIndex:0]] andUserId:appMan.coordinator.credentials.userId];
                callbackBlock(@"success",contact);
            } Failure:^{
                NSLog(@"FAILURE GET ACCOUNTS");
                failure();
            }];
        }Failure:^{
            NSLog(@"FAIL CREATE REQUEST FOR CONTACTS");
            failure();
        }];*/
        return nil;
    }
}

-(void) deleteContact: (NSString *)objectType WithId:(NSString *)objectId{
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForDeleteWithObjectType:objectType objectId:objectId];
    [[SFRestAPI sharedInstance] send:request delegate:self];
    NSLog(@"errorMessage0 = %@",errorMessage);
    
}
//- (void)request:(SFRestRequest*)request didFailLoadWithError:(NSError *)error
//{
//    NSLog(@"Error!!!");
//    NSLog(@"!!! = %@",[error.userInfo objectForKey:@"errorCode"]);
//    NSLog(@"Description = %@",[error.userInfo objectForKey:@"message"]);
//    errorMessage = [error.userInfo objectForKey:@"errorCode"];
//
//    
//    [[ContactsController new]getStatus:errorMessage WithMessage:[error.userInfo objectForKey:@"message"]];
//   // [[iPad_AccountsViewController new]getStatusAccount:errorMessage WithMessage:[error.userInfo objectForKey:@"message"]];
//    
//}

//- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse
//{
//    NSLog(@"Success!!!");
//    NSLog(@"!!! = %@",jsonResponse);
//    errorMessage = @"false";
//    
//    [[ContactsController new]getStatus:errorMessage WithMessage:nil];
//  //  [[iPad_AccountsViewController new]getStatusAccount:errorMessage WithMessage:nil];
//}


-(void) addContactOrAccount: (NSString *)objectType dictionary:(NSMutableDictionary *)field{
    
//    NSMutableDictionary * fields = [[NSMutableDictionary alloc] init];
//   [fields setObject:@"svetlana" forKey:@"Name"];
    
//   [fields setObject:@"Call" forKey:@"Subject"];
//   [fields setObject:@"2013-08-29" forKey:@"Last Modified By"];
//   [fields setObject:@"svetlana" forKey:@"Assigned To"];


         SFRestRequest *request = [[SFRestAPI sharedInstance] requestForCreateWithObjectType:objectType fields:field];
        [[SFRestAPI sharedInstance] send:request delegate:self];
    
}
-(void) editContact: (NSString *)objectType WithKeyId:(NSString *)objectId dictionary:(NSMutableDictionary *)field
{
     SFRestRequest *saveRequest = [[SFRestAPI sharedInstance] requestForUpdateWithObjectType:objectType objectId:objectId fields:field];
    [[SFRestAPI sharedInstance] send:saveRequest delegate:self];
}

//-(void) addContact: (Contact *)contact Callback: (void (^)(NSString*))callbackBlock Failure:(void (^)())failure{
//    NSString *objectType = @"Contact";
//
//    NSMutableDictionary * fields = [[NSMutableDictionary alloc] init];
////    [fields setObject:contact.idKey forKey:@"Id"];
////    [fields setObject:contact.isDeleted forKey:@"IsDeleted"];
////    [fields setObject:contact.masterRecordId forKey:@"MasterRecordId"];
////    [fields setObject:contact.accountId forKey:@"AccountId"];
//    [fields setObject:contact.lastName forKey:@"LastName"];
//    [fields setObject:contact.firstName forKey:@"FirstName"];
////    [fields setObject:contact.salutation forKey:@"Salutation"];
////    [fields setObject:contact.otherStreet forKey:@"OtherStreet"];
////    [fields setObject:contact.otherCity forKey:@"OtherCity"];
////    [fields setObject:contact.otherState forKey:@"OtherState"];
////    [fields setObject:contact.otherPostalCode forKey:@"OtherPostalCode"];
////    [fields setObject:contact.otherCountry forKey:@"OtherCountry"];
////    [fields setObject:contact.mailingStreet forKey:@"MailingStreet"];
////    [fields setObject:contact.mailingCity forKey:@"MailingCity"];
////    [fields setObject:contact.mailingState forKey:@"MailingState"];
////    [fields setObject:contact.mailingPostalCode forKey:@"MailingPostalCode"];
////    [fields setObject:contact.mailingCountry forKey:@"MailingCountry"];
//    [fields setObject:contact.phone forKey:@"Phone"];
//    [fields setObject:contact.fax forKey:@"Fax"];
//    [fields setObject:contact.mobilePhone forKey:@"MobilePhone"];
//    [fields setObject:contact.homePhone forKey:@"HomePhone"];
////    [fields setObject:contact.otherPhone forKey:@"OtherPhone"];
////    [fields setObject:contact.assistantPhone forKey:@"AssistantPhone"];
////    [fields setObject:contact.reportsToId forKey:@"ReportsToId"];
//    [fields setObject:contact.email forKey:@"Email"];
//    [fields setObject:contact.title forKey:@"Title"];
////    [fields setObject:contact.department forKey:@"Department"];
////    [fields setObject:contact.assistantName forKey:@"assistantName"];
////    [fields setObject:contact.leadSource forKey:@"LeadSource"];  
////    [fields setObject:contact.birthdate forKey:@"Birthdate"];
////    [fields setObject:contact.description forKey:@"Description"];
////    [fields setObject:contact.ownerId forKey:@"OwnerId"];
////    [fields setObject:contact.createdDate forKey:@"CreatedDate"];
////    [fields setObject:contact.createdById forKey:@"CreatedById"];
////    [fields setObject:contact.lastModifiedDate forKey:@"LastModifiedDate"];
////    [fields setObject:contact.lastModifiedById forKey:@"LastModifiedById"];
////    [fields setObject:contact.systemModstamp forKey:@"SystemModstamp"];
////    [fields setObject:contact.lastActivityDate forKey:@"LastActivityDate"];
////    [fields setObject:contact.lastCURequestDate forKey:@"LastCURequestDate"];
////    [fields setObject:contact.lastCUUpdateDate forKey:@"LastCUUpdateDate"];
////    [fields setObject:contact.emailBouncedReason forKey:@"EmailBouncedReason"];   
////    [fields setObject:contact.emailBouncedDate forKey:@"EmailBouncedDate"];
////    [fields setObject:contact.jigsaw forKey:@"Jigsaw"];
////    [fields setObject:contact.jigsawContactId forKey:@"JigsawContactId"];
//
//    //prep actual operation
//    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForCreateWithObjectType:objectType fields:field];
//
//    //send the request
//    [[SFRestAPI sharedInstance] send:request delegate:self];
//    
//}


- (void) createRequestContacts: (void (^)(NSString*))callbackBlock Failure:(void (^)())failure{
    SFRestRequest *leadsStructureRequest = [[SFRestAPI sharedInstance] requestForDescribeWithObjectType:@"Contact"];
    
    [slQuery getInfoForSalesforceURL:[NSString stringWithFormat:@"%@%@",leadsStructureRequest.endpoint, leadsStructureRequest.path] params:leadsStructureRequest.queryParams Response:^(id JSON) {       
        NSDictionary *fields = [JSON objectForKey:@"fields"];
        NSString * toSelect = @"";
        
        for(NSDictionary *name in fields){
           if([toSelect isEqualToString:@""]){
               toSelect = [toSelect stringByAppendingFormat:@"%@",[name objectForKey:@"name"]];
            } else {
                toSelect = [toSelect stringByAppendingFormat:@", %@",[name objectForKey:@"name"]];
            }
        }
        
        callbackBlock(toSelect);
    } Failure:^{
        callbackBlock(@"");
    }];
}



@end

