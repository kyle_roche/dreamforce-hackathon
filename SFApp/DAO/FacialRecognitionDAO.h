//
//  FacialRecognitionDAO.h
//  DreamforceHackathon
//
//  Created by Vadim on 28.10.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacialRecognitionDAO : NSObject

+ (FacialRecognitionDAO *)instance;

- (void)sendRequestWithUrl:(NSURL*)requestURL withImageData:(NSData*)imageData withParams:(NSDictionary*)params withUsername:(NSString*)username withPassword:(NSString*)password Response:(void (^)(id response))callbackBlock Failure:(void (^)())failure;

- (void)uploadImageToServer:(NSData*)imageData withParams:(NSDictionary*)params Response:(void (^)(NSString* status,NSDictionary* data))callbackBlock Failure:(void (^)())failure;
- (void)runFaceRecognitionForImage:(NSData*)imageData Response:(void (^)(NSString* status,NSArray* data))callbackBlock Failure:(void (^)())failure;

@end
