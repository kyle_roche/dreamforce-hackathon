//
//  AppManager.m
//  Playbook
//
//  Created by Julia on 16.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import "AppManager.h"
//#import "RDContactManager.h"

@implementation AppManager

static AppManager *_manager = nil;

@synthesize loginDone;
@synthesize SFDC_userId = _SFDC_userId;

+ (AppManager *)instance {
    if (!_manager) {
        _manager = [[AppManager alloc] init];
        [_manager initData];
    }
    return _manager;
}

- (void) initData{


}

-(id)init{
    
    self = [super init];
    if(self) {
        _contactsArray = [[NSMutableArray alloc ] init];
        _sfdcIdToSensorId = [[NSDictionary alloc] initWithObjectsAndKeys:@"GC8X-1K7U4",@"003i000000L7rYuAAJ",
                             @"GC8X-1K7U4",@"003i000000L7rYvAAJ",
                             @"NCV4-9YUBZ",@"003i000000L7rYwAAJ",
                             @"Z2ZG-HQK9K",@"003i000000L7rYxAAJ",
                             @"GC8X-1K7U4",@"003i000000L7rYyAAJ",
                             @"Z2ZG-HQK9K",@"003i000000L7rYzAAJ",
                             @"NCV4-9YUBZ",@"003i000000L7rZ0AAJ",
                             @"NCV4-9YUBZ",@"003i000000L7rZ1AAJ",
                             @"GC8X-1K7U4",@"003i000000L7rZ2AAJ",
                             @"GC8X-1K7U4",@"003i000000L7rZ3AAJ",
                             @"Z2ZG-HQK9K",@"003i000000L7rZ4AAJ",
                             @"GC8X-1K7U4",@"003i000000L7rZ5AAJ",
                             @"Z2ZG-HQK9K",@"003i000000L7rZ6AAJ",
                             @"GC8X-1K7U4",@"003i000000L7rZ7AAJ",
                             @"GC8X-1K7U4",@"003i000000L7rZ8AAJ",
                             @"GC8X-1K7U4",@"003i000000L7rZ9AAJ",
                             @"NCV4-9YUBZ",@"003i000000L7rZAAAZ",
                             @"GC8X-1K7U4",@"003i000000L7rZBAAZ",
                             @"Z2ZG-HQK9K",@"003i000000L7rZCAAZ",
                             @"Z2ZG-HQK9K",@"003i000000L7rZDAAZ", nil];
    }
    
    return self;
}


-(NSDictionary*) checkNullWithDict:(NSDictionary*)dataDict{
    NSMutableDictionary *tmpNewDict = [[NSMutableDictionary alloc] init];
    for (NSString *tmpKey in dataDict){
        [tmpNewDict setObject:[self checkNullWithString:[dataDict objectForKey:tmpKey]] forKey:tmpKey];
    }
    return (NSDictionary*)tmpNewDict;
}

-(NSString*)checkNullWithString:(NSString*)str{
    if ([str isKindOfClass:[NSNull class]]){
        return  @" ";
    } else {
        return [NSString stringWithFormat:@"%@", str];
    }
}


//- (void) clear
//{
//     _SFDC_userId = @"";
//    self.loginDone = NO;
//    [[RDContactManager sharedManager] resetManager];
//}
//
//- (RDContactManager*)contactManager {
//    return [RDContactManager sharedManager];
//}
//
//- (MatchingContactsStorage*)contactStore {
//    return [[self contactManager] contactStore];
//}

- (void)dealloc {

    _SFDC_userId = nil;
}

- (NSURL*) getGravatarURL:(NSString*) emailAddress
{
	NSString *curatedEmail = [[emailAddress stringByTrimmingCharactersInSet:
							   [NSCharacterSet whitespaceCharacterSet]]
							  lowercaseString];
    
	const char *cStr = [curatedEmail UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result); // compute MD5
    
	NSString *md5email = [NSString stringWithFormat:
                          @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                          result[0], result[1], result[2], result[3],
                          result[4], result[5], result[6], result[7],
                          result[8], result[9], result[10], result[11],
                          result[12], result[13], result[14], result[15]
                          ];
	NSString *gravatarEndPoint = [NSString stringWithFormat:@"http://www.gravatar.com/avatar/%@?s=512", md5email];
    
	return [NSURL URLWithString:gravatarEndPoint];
}

-(NSDate*)adjustmentDateFromDate:(NSString*)format date:(NSString*)srcDate{
    NSDateFormatter *dateFormatter;
    NSLocale * enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] ;
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *dstDate = [dateFormatter dateFromString:srcDate];
    return dstDate;
}

- (NSString*)localDateFromDate:(NSString*)srcformat dstFormat:(NSString*)dstFormat date:(NSString*)srcDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:srcformat];
    NSDate * dateS = [dateFormatter dateFromString:srcDate];
    dateFormatter.dateFormat = dstFormat;
    NSString *dstDate = [dateFormatter stringFromDate:dateS];
    return dstDate;
}

@end
