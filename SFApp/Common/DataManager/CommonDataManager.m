//
//  CommonDataManager.m
//  sports-iOS-Program-Rapids
//
//  Created by Elena on 5/13/13.
//
//

#import "CommonDataManager.h"

@implementation CommonDataManager
@synthesize managedObjectContext = _managedObjectContext;

static  CommonDataManager *commonDataManager = nil;


+ (CommonDataManager *)instance {
    if (!commonDataManager) {
        commonDataManager = [[CommonDataManager alloc] init];

    }
    return commonDataManager;
}

-(id)init {
    self = [super init];
    if(self) {
        _daoQueue = [[NSOperationQueue alloc] init];
        _daoQueue.maxConcurrentOperationCount = 1;
        
        _galleryImagesQueue = [[NSOperationQueue alloc] init];
        _galleryImagesQueue.maxConcurrentOperationCount = 1;

        _galleryImagesOperations = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName {
    NSFetchRequest * request = [[NSFetchRequest alloc] init] ;
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext]];
    
    NSError *error = nil;
    //NSLog(@"request = %@", request);
    [request setReturnsObjectsAsFaults:NO];
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    //NSLog(@"result = %@", results);
    return results;
}

-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName withRequest:(NSString*)requestStr {
    NSFetchRequest * request = [[NSFetchRequest alloc] init] ;
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext]];
    
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:requestStr];
    [request setPredicate:searchFilter];
    NSError *error = nil;
    [request setReturnsObjectsAsFaults:NO];
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    return results;
}

-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName FromContext:(NSManagedObjectContext*)context {
    NSFetchRequest * request = [[NSFetchRequest alloc] init] ;
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    [request setReturnsObjectsAsFaults:NO];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    return results;
}

-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName withRequest:(NSString*)requestStr inContext:(NSManagedObjectContext*) context {
    NSFetchRequest * request = [[NSFetchRequest alloc] init] ;
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:requestStr];
    [request setPredicate:searchFilter];
    NSError *error = nil;
    [request setReturnsObjectsAsFaults:NO];
    NSArray *results = [context executeFetchRequest:request error:&error];
    return results;
}

-(void)removeModelsFromCoreDataEntity:(NSString *)entityName WithRequest:(NSString*)requestStr {
    //NSLog(@"removeModelsFromCoreDataEntity = %@", entityName);
    NSArray *results = [self getAllModelsFromCoreData:entityName withRequest:requestStr];
    for (int i=0; i<results.count; i++) {
        [self.managedObjectContext deleteObject:[results objectAtIndex:i]];
    }
    [self.managedObjectContext save:nil];
}

-(void)removeModelsFromCoreDataEntityNoSave:(NSString*)entityName{
    NSArray *results = [self getAllModelsFromCoreData:entityName];
    for (int i=0; i<results.count; i++) {
        [self.managedObjectContext deleteObject:[results objectAtIndex:i]];
    }
}

-(void)removeModelsFromCoreDataNoSave:(NSArray*)models FromContext:(NSManagedObjectContext*) context{
    for (int i=0; i<models.count; i++) {
        [context deleteObject:[models objectAtIndex:i]];
    }
}

-(void)removeModelsFromCoreDataEntity:(NSString*)entityName{
    //NSLog(@"removeModelsFromCoreDataEntity = %@", entityName);
    NSArray *results = [self getAllModelsFromCoreData:entityName];
    for (int i=0; i<results.count; i++) {
        [self.managedObjectContext deleteObject:[results objectAtIndex:i]];
    }
    [self.managedObjectContext save:nil];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = _managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectContext*)createNewContextForCurrentThread {
    
    NSManagedObjectContext * context = nil;
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        //[context setPersistentStoreCoordinator:[self persistentStoreCoordinator]];
        [context setParentContext:[self managedObjectContext]];
    }
    return context;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"sfdn-CoreData" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"sfdn-CoreData.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
    }
    
    return _persistentStoreCoordinator;
}
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
