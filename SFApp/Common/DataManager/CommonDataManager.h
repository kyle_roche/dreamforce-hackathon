//
//  CommonDataManager.h
//  sports-iOS-Program-Rapids
//
//  Created by Elena on 5/13/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CommonDataManager : NSObject
+ (CommonDataManager *)instance;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSOperationQueue *    daoQueue;

@property (nonatomic, strong) NSOperationQueue *    galleryImagesQueue;
@property (nonatomic, strong) NSMutableDictionary *    galleryImagesOperations;

- (void)saveContext;
-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName;
-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName withRequest:(NSString*)requestStr;
-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName FromContext:(NSManagedObjectContext*)context;
-(void)removeModelsFromCoreDataEntity:(NSString*)entityName;
-(void) removeModelsFromCoreDataEntityNoSave:(NSString*)entityName;
-(void)removeModelsFromCoreDataNoSave:(NSArray*)models FromContext:(NSManagedObjectContext*) context;
- (NSManagedObjectContext*)createNewContextForCurrentThread;
-(NSArray*) getAllModelsFromCoreData:(NSString*)entityName withRequest:(NSString*)requestStr inContext:(NSManagedObjectContext*) context;
@end
