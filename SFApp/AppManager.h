//
//  AppManager.h
//  Playbook
//
//  Created by Julia on 16.08.13.
//  Copyright (c) 2013 Светлана. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFOAuthCoordinator.h"
//#import "RDContactManager.h"

@interface AppManager : NSObject

+ (AppManager *)instance;
- (void)initData;
- (NSDictionary*) checkNullWithDict:(NSDictionary*)dataDict;

@property (nonatomic, retain) NSString *SFDC_userId;
@property (nonatomic, retain) SFOAuthCoordinator *coordinator;
//@property (readonly) RDContactManager *contactManager;
//@property (readonly) MatchingContactsStorage * contactStore;


@property (assign) BOOL loginDone;
@property (nonatomic, assign) int index;
@property (nonatomic, assign)  NSIndexPath *indexPath;


@property (nonatomic, strong) NSMutableArray * contactsArray;
@property (nonatomic, strong) NSDictionary * sfdcIdToSensorId;

- (NSURL*)getGravatarURL:(NSString*) emailAddress;
- (NSString*)localDateFromDate:(NSString*)srcformat dstFormat:(NSString*)dstFormat date:(NSString*)srcDate;
-(NSDate*)adjustmentDateFromDate:(NSString*)format date:(NSString*)srcDate;

@end
