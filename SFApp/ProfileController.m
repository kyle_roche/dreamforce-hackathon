//
//  ProfileController.m
//  SFApp
//
//  Created by Мария on 02.09.13.
//  Copyright (c) 2013 2lemetry. All rights reserved.
//

#import "ProfileController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppManager.h"
#import "CommonDataManager.h"
#import "ContactDAO.h"
//#import "AmazonDAO.h"
#import "FacialRecognitionDAO.h"

@interface ProfileController ()
{
    AppManager *appMan;
    UIActivityIndicatorView *progress;
    UIAlertView *alert;
}

@end

@implementation ProfileController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appMan = [AppManager instance];
	// Do any additional setup after loading the view.
    _swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    _swipeGesture.numberOfTouchesRequired = 1;
    _swipeGesture.direction = (UISwipeGestureRecognizerDirectionRight);
    [self.view addGestureRecognizer:_swipeGesture];
    
    [self showInfoAboutContact:_contact];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void) dismissAlert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    alert = nil;
}

- (void)swipedScreen:(UISwipeGestureRecognizer*)gesture {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showInfoAboutContact:(Contact*)contact
{
    _profileImage.contentMode = UIViewContentModeScaleAspectFill;
    _profileImage.clipsToBounds = YES;
    _profileImage.layer.cornerRadius = 48.0f;
    _profileImage.layer.borderWidth = 4.0f;
    _profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [_profileImage setImage:[[UIImage alloc] initWithData:contact.avatarImageData]];
    _contactNameLabel.text = contact.name;
    _contactEmailLabel.text = contact.email;
    _titleLabel.text = contact.title;
    _phoneLabel.text = contact.phone;
    _mobileLabel.text = contact.mobilePhone;
    _faxLabel.text = contact.fax;
    _addressLabel.text = [NSString stringWithFormat:@"%@ %@", contact.mailingStreet, contact.mailingCity];
    
    _sensorIdField.text = contact.sensorID;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillHide:(NSNotification *)n
{
    
        CGRect viewFrame = self.infoScroll.frame;
        //viewFrame.size.height += 160;
        [UIView beginAnimations:@"fieldsDown" context:NULL];
        [self.infoScroll setContentSize:viewFrame.size];
        [self.infoScroll setContentOffset:CGPointMake(0, 0)];
        [UIView commitAnimations];

    
}

- (void)keyboardWillShow:(NSNotification *)n
{

        CGRect viewFrame = self.infoScroll.frame;
        viewFrame.size.height += 230;
        [UIView beginAnimations:@"fieldsUp" context:NULL];
        [self.infoScroll setContentSize:viewFrame.size];
        [self.infoScroll setContentOffset:CGPointMake(0, 230)];
        [UIView setAnimationDuration: 2.0];
        [UIView commitAnimations];

    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)changeSensorId:(id)sender {
    _contact.sensorID = _sensorIdField.text;
    [[CommonDataManager instance].managedObjectContext save:nil];
    //[[ContactDAO instance] editContact:@"Contact" WithKeyId:_contact.idKey dictionary:[[NSMutableDictionary alloc] initWithObjectsAndKeys:_sensorIdField.text,@"SensorId__c",nil]];
    
    NSString *sensorID;
    sensorID = _contact.sensorID;
    NSLog(@"sensorID = %i",sensorID.length);
    if(sensorID.length==0){
        
        sensorID= @" ";
    }

    

}

- (IBAction)addPhotoButtonClick:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Photo Library",@"Camera", nil];
    actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self photoLibraryButtonClick];
        
    } else if (buttonIndex == 1){
        [self cameraButtonClick];
    }

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];

    UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    //self.profileImage.image = image;
    
    alert = [[UIAlertView alloc] initWithTitle: @"Uploading..." message: nil delegate:self cancelButtonTitle: nil otherButtonTitles: nil];
    progress= [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(125, 50, 30, 30)];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [alert addSubview: progress];
    [progress startAnimating];
    [alert show];
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:_contact.idKey forKey:@"sfId"];
    [params setObject:_contact.firstName forKey:@"firstName"];
    [params setObject:_contact.lastName forKey:@"lastName"];
    
    [[FacialRecognitionDAO instance] uploadImageToServer:UIImageJPEGRepresentation(image, 1.0) withParams:(NSDictionary *)params Response:^(NSString *status, NSDictionary *data) {
        if ([status isEqualToString:@"progress"])
        {
            [alert setTitle:[NSString stringWithFormat:@"Uploading...%d%%", [[data objectForKey:@"progress"] intValue]]];
        }
        else
        {
            NSLog(@"upload %@: response = %@", status, data);
            [progress stopAnimating];
            [alert setTitle:@"Upload successful"];
            progress.hidden = YES;
            [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:1.0];
        }
    } Failure:^{
        NSLog(@"upload fail!");
        [progress stopAnimating];
        [alert setTitle:@"Upload fail"];
        progress.hidden = YES;
        [self performSelector:@selector(dismissAlert) withObject:nil afterDelay:1.0];
    }];
}

- (void)photoLibraryButtonClick {
    UIImagePickerController *imagePicker = [[UIImagePickerController     alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:^{

    }];
}

- (void)cameraButtonClick {
    UIImagePickerController *imagePicker = [[UIImagePickerController     alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:^{

    }];
}

@end
